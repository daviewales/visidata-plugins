# VisiData Plugins

Please see the [official instructions](https://www.visidata.org/docs/plugins/) to install VisiData plugins!


## Vega-lite saver

Save Vega-lite spec with embedded data from current sheet. Use ` to set the
encoding channels for the current column.

See:
[![asciicast](https://asciinema.org/a/3Y1kp50U1P9DY6NBIHzJko1BB.svg)](https://asciinema.org/a/3Y1kp50U1P9DY6NBIHzJko1BB)


## Compare Columns

NOTE: You must open each sheet at least once to ensure that the sheet is loaded into VisiData.
Otherwise this plugin won't find any column headers when it goes looking!

Adds the command `compare-columns`.
Creates a sheet with the headers of each table in consecutive columns.

![Select Sheets](images/compare-columns-sheet-selection.png)

![Compare Columns](images/compare-columns.png)

