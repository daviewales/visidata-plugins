"""
# Usage

On an IndexSheet such as the SheetsSheet, select some tables to compare column headers.

## Commands

- `show-columns` extracts the column headers from each selected table, and displays them side by side.
- `diff-columns` is the same as `show-columns`, but also applies a visual diff (like vimdiff)
"""


__version__ = "0.1.0"
__author__ = "David Wales <daviewales@disroot.org>"

from difflib import SequenceMatcher
from itertools import zip_longest
from visidata import vd, asyncthread, Progress, Sheet, TableSheet, SequenceSheet, Column


class CompareColumnsSheet(SequenceSheet):
    rowtype = 'fields'  # rowdef: [str]


    def cols_of_selected_tables(self):
        selected_tables = self.source.selectedRows
        table_columns = {}
        for table in selected_tables:
            table_columns[table.name] = [column.name for column in table.columns]
        return table_columns


    def cols_of_selected_tables_with_headers(self):
        table_columns = self.cols_of_selected_tables()
        return [[table] + columns for table, columns in table_columns.items()]


    def iterload(self):
        table_columns_with_headers = self.cols_of_selected_tables_with_headers()

        for line in zip_longest(*table_columns_with_headers):
            yield line


@Sheet.api
def compare_columns(sheet):
    compare_sheet = CompareColumnsSheet("compare-columns-sheet", source=sheet)
    vd.push(compare_sheet)

Sheet.addCommand(None, "compare-columns", "vd.sheet.compare_columns()")


class DiffColumnsSheet(CompareColumnsSheet):
    rowtype = 'fields'  # rowdef: [str]


    def iterload(self):
        table_columns = list(self.cols_of_selected_tables().values())
        table_columns_with_headers = self.cols_of_selected_tables_with_headers()

        a = table_columns[0]
        b = table_columns[1]
        sequence_matcher = SequenceMatcher(a=a, b=b)
        opcodes = sequence_matcher.get_opcodes()

        a2 = []
        b2 = []
        for op in opcodes:
            if op[0] == 'equal':
                a2.extend(a[op[1]:op[2]])
                b2.extend(b[op[3]:op[4]])
            elif op[0] == 'insert':
                a2.append('')
                b2.extend(b[op[3]:op[4]])
            elif op[0] == 'delete':
                a2.extend(a[op[1]:op[2]])
                b2.append('')
            elif op[0] == 'replace':
                a2.extend(a[op[1]:op[2]])
                b2.extend(b[op[3]:op[4]])

        table_columns[0] = [table_columns_with_headers[0][0]] + a2
        table_columns[1] = [table_columns_with_headers[1][0]] + b2

        with open('test-file.txt', 'a') as testfile:
            print(sequence_matcher.get_opcodes(), file=testfile)

        for line in zip_longest(*table_columns):
            yield line

@Sheet.api
def diff_columns(sheet):
    diff_sheet = DiffColumnsSheet("diff-columns-sheet", source=sheet)
    vd.push(diff_sheet)

Sheet.addCommand(None, "diff-columns", "vd.sheet.diff_columns()")
