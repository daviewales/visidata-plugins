import json

from visidata import vd, VisiData, Progress, Sheet, choose, ENTER, options, ReturnValue, CompleteKey

vegalite_spec = {
    "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
    "description": "A simple chart",
    "width": 360,
    "data": {
         # get values from sheet
         "values": []
    },
    "mark": "bar",
    # set encoding from column type and vegalite_encoding_channels property
    "encoding": {}
 }


vegalite_template_start = """{
  "$schema": "https://vega.github.io/schema/vega-lite/v5.json",
  "description": "A simple bar chart with embedded data.",
  "width": 360,
  "data": {
    "values": [
"""

      # {"a": "A","b": 28}, {"a": "B","b": 55}, {"a": "C","b": 43},
      # {"a": "D","b": 91}, {"a": "E","b": 81}, {"a": "F","b": 53},
      # {"a": "G","b": 19}, {"a": "H","b": 87}, {"a": "I","b": 52}

vegalite_template_middle = """    ]
  },
  "mark": "bar",
  "encoding": """

vegalite_template_encoding = """{
    "x": {"field": "a", "type": "ordinal"},
    "y": {"field": "b", "type": "quantitative"},
    "tooltip": {"field": "b", "type": "quantitative"}
  }"""

vegalite_template_end = """
}"""


class _vjsonEncoder(json.JSONEncoder):
    def default(self, obj):
        return str(obj)


def get_rows(sheet, cols):
    jsonenc = _vjsonEncoder()
    for index, row in enumerate(Progress(sheet.rows)):
        row_json = jsonenc.encode({col.name: col.getTypedValue(row) for col in cols})
        indent = 6*" "
        if index == sheet.nRows - 1:
            # no trailing comma on last line
            yield indent + row_json + '\n'
        else:
            yield indent + row_json + ',\n'


# def vegalite_encoding_channels_set(col, channels):
#     col.vegalite_encoding_channels = channels

# https://vega.github.io/vega-lite/docs/encoding.html

@VisiData.property
def vegalite_encoding_channel_choices(vd):
    choices = [
        # Position Channels
        {'key': "x", 'desc': ''},
        {'key': "y" , 'desc': ''},
        {'key': "x2" , 'desc': ''},
        {'key': "y2" , 'desc': ''},
        {'key': "xError" , 'desc': ''},
        {'key': "yError" , 'desc': ''},
        {'key': "xError2" , 'desc': ''},
        {'key': "yError2" , 'desc': ''},

        # Polar Position Channels
        {'key': "theta" , 'desc': ''},
        {'key': "radius" , 'desc': ''},
        {'key': "theta2" , 'desc': ''},
        {'key': "radius2" , 'desc': ''},

        # Geographic Position Channels
        {'key': "longitude" , 'desc': ''},
        {'key': "latitude" , 'desc': ''},
        {'key': "longitude2" , 'desc': ''},
        {'key': "latitude2" , 'desc': ''},

       # Mark Properties Channels
       {'key': "color" , 'desc': ''},
       {'key': "opacity" , 'desc': ''},
       {'key': "fillOpacity" , 'desc': ''},
       {'key': "strokeOpacity" , 'desc': ''},
       {'key': "strokeWidth" , 'desc': ''},
       {'key': "strokeDash" , 'desc': ''},
       {'key': "size" , 'desc': ''},
       {'key': "angle" , 'desc': ''},
       {'key': "shape" , 'desc': ''},

       # Text and Tooltip Channels
       {'key': "text" , 'desc': ''},
       {'key': "tooltip" , 'desc': ''},

       # Hyperlink Channel
       {'key': "href" , 'desc': ''},

       # Description Channel
       {'key': "description" , 'desc': ''},

       # Level of Detail Channel
       {'key': "detail" , 'desc': ''},

       # Key Channel
       {'key': "key" , 'desc': ''},

       # Order Channel
       {'key': "order" , 'desc': ''},

       # Facet Channels
       {'key': "facet" , 'desc': ''},
       {'key': "row" , 'desc': ''},
       {'key': "column" , 'desc': ''},
    ]
    return choices


@VisiData.api
def chooseSome(vd, choices):
    '''This is a modified version of the built-in vd.chooseMany() function.
    Unlike the built-in chooseMany() function, chooseSome() won't select
    multiple keys which share a prefix when a prefix is entered. This prevents
    us from selecting both `x` and `x2` (both valid vega-lite encodings) when
    `x` is entered.

    Return a list of 1 or more keys from *choices*, which is a list of dicts.
    Each element dict must have a unique "key", which must be typed directly by
    the user in non-fancy mode (therefore no spaces). All other items in the
    dicts are also shown in fancy chooser mode. Use previous choices from the
    replay input if available. Add chosen keys (space-separated) to the cmdlog
    as input for the current command.'''

    if vd.cmdlog:
        v = vd.getLastArgs()
        if v is not None:
            # check that each key in v is in choices?
            vd.setLastArgs(v)
            return v.split()

    if options.fancy_chooser:
        chosen = vd.chooseFancy(choices)
    else:
        chosen = []
        choice_keys = [c['key'] for c in choices]
        prompt='choose any of %d options (Ctrl+X for menu)' % len(choice_keys)
        try:
            def throw_fancy(v, i):
                ret = vd.chooseFancy(choices)
                if ret:
                    raise ReturnValue(ret)
                return v, i
            chosenstr = vd.input(prompt+': ', completer=CompleteKey(choice_keys), bindings={'^X': throw_fancy})
            for c in chosenstr.split():
                if c in choice_keys:
                    chosen.append(c)
                else:
                    vd.warning('invalid choice "%s"' % c)
        except ReturnValue as e:
            chosen = e.args[0]

    if vd.cmdlog:
        vd.setLastArgs(' '.join(chosen))

    return chosen


choose.ChoiceSheet.addCommand(ENTER, 'choose-rows', 'makeChoice([cursorRow])')
choose.ChoiceSheet.addCommand('g'+ENTER, 'choose-rows-selected', 'makeChoice(onlySelectedRows)')


@Sheet.api
def addVegaLiteEncodings(sheet, cols, encoding_names):
    'Add each vega-lite encoding channel in list of `encoding_names` to each of `cols`.'
    for encoding_name in encoding_names:
        for col in cols:
            if not hasattr(col, 'vegalite_encoding_channels'):
                col.vegalite_encoding_channels = []
            if encoding_name and encoding_name not in col.vegalite_encoding_channels:
                col.vegalite_encoding_channels.append(encoding_name)


def map_vegalite_type(vd_type_name):
    if not vd_type_name:
        return 'nominal'
    else:
        type_map = {
            'str': 'nominal',
            'date': 'temporal',
            'int': 'quantitative',
            'float': 'quantitative',
            'currency': 'quantitative',
            'vlen': 'quantitative',
        }
        return type_map[vd_type_name]


def outputVegaLiteEncodings(sheet):
    '''Return vegalite encoding channels for sheet.'''
    encoding = {}
    for col in sheet.columns:
        if hasattr(col, 'vegalite_encoding_channels'):
            for enc in col.vegalite_encoding_channels:
                # This assumes we have only set one of each type of encoding.
                # For example, if you set the `x` encoding on multiple columns,
                # it will only appear on one.
                encoding[enc] = {"field": col.name, "type": map_vegalite_type(col.type.__name__)}
    jsonenc = _vjsonEncoder(indent=4)
    return jsonenc.encode(encoding)


@VisiData.api
def save_vegalite(vd, path, *sheets):
    with path.open_text(mode='w') as fp:
        for sheet in sheets:
            # TODO: Use dicts and jsonencoder to generate JSON output rather than manually
            # formatting JSON strings...
            # But try to keep the data saving asynchronous.
            fp.write(vegalite_template_start)
            for row in get_rows(sheet, sheet.visibleCols):
                fp.write(row)
            fp.write(vegalite_template_middle)
            fp.write(outputVegaLiteEncodings(sheet))
            fp.write(vegalite_template_end)


Sheet.addCommand('`', 'vegalite-encode-column',
                 'addVegaLiteEncodings([cursorCol], chooseSome(vegalite_encoding_channel_choices))',
                 'Add vegalite encoding to current column')
Sheet.addCommand('g`', 'show-vegalite-column-encodings', 'vd.status(cursorCol.vegalite_encoding_channels)', 'Show vegalite encoding channels')
